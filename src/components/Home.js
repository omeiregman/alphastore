import React from 'react';
import { Link } from 'react-router-dom';
import './css/main.css';


import img_film1 from './images/film/film1.jpg';
import img_film2 from './images/film/2.jpg';
import img_film3 from './images/film/3.jpg';
import img_film4 from './images/film/film4.jpg';
import img_film5 from './images/film/film5.jpg';
import img_film6 from './images/film/film6.jpg';
import img_film7 from './images/film/film7.jpg';
import img_film8 from './images/film/fim7.jpg';
import img_film9 from './images/film/fim12.jpg';
import img_film10 from './images/film/fim5.jpg';
import img_film11 from './images/film/fim4.jpg';
import img_film12 from './images/film/fim3.jpg';



class Home extends React.Component {
    render() {
        return(
            <div>
            <main>
            <div className="jumbotron main-jumbo"> 
              <h1>Film Rentals Made Easy</h1>
                <a to="/movie" className="btn btn-primary">Watch Now</a>
               </div>
               <div className="container-fluid">
                 <h2 id= "movies">New To Rent</h2>
                  <div className="row">
                      <div className="col-md-3">
                          <div className="card mb-3 box-shadow">
                              <img className="card-img-top" src={img_film1} alt="Card image cap"/>
                              </div>
                            </div>
                      <div className="col-md-3">
                          <div className="card mb-3 box-shadow">
                             <img className="card-img-top" src={img_film2} alt="Card image cap"/>
                              </div>
                                 </div>
                      <div className="col-md-3">
                          <div className="card mb-3 box-shadow">
                            <img className="card-img-top" src={img_film3} alt="Card image cap"/>
                              </div>
                              </div>
                             <div className="col-md-3">
                          <div className="card mb-3 box-shadow">
                            <img className="card-img-top" src={img_film4} alt="Card image cap"/>
                              </div>
                              </div>
                             </div>
                    <div className="row">
                      <div className="col-md-3">
                          <div className="card mb-3 box-shadow">
                              <img className="card-img-top" src={img_film5} alt="Card image cap"/>
                              </div>
                            </div>
                      <div className="col-md-3">
                          <div className="card mb-3 box-shadow">
                             <img className="card-img-top" src={img_film6} alt="Card image cap"/>
                              </div>
                                 </div>
                      <div className="col-md-3">
                          <div className="card mb-3 box-shadow">
                            <img className="card-img-top" src={img_film7} alt="Card image cap"/>
                              </div>
                              </div>
                             <div className="col-md-3">
                          <div className="card mb-3 box-shadow">
                            <img className="card-img-top" src={img_film8} alt="Card image cap"/>
                              </div>
                              </div>
                             </div>
                    <div className="row">
                      <div className="col-md-3">
                          <div className="card mb-3 box-shadow">
                              <img className="card-img-top" src={img_film9} height="" alt="Card image cap"/>   
                               </div>
                                  </div>
                       <div className="col-md-3">
                          <div className="card mb-3 box-shadow">
                              <img className="card-img-top" src={img_film10} height="" alt="Card image cap"/>
                          </div>
                        </div>
                        <div className="col-md-3">
                          <div className="card mb-3 box-shadow">
                              <img className="card-img-top" src={img_film11} height="" alt="Card image cap"/>
                          </div>
                        </div>
                        <div className="col-md-3">
                          <div className="card mb-3 box-shadow">
                              <img className="card-img-top" src={img_film12} height="" alt="Card image cap"/>
                        </div>
                      </div>
                    </div>
                     <div className="row">
                      <div className="col-md-3">
                          <div className="card mb-3 box-shadow">
                              <img className="card-img-top" src={img_film1} height="" alt="Card image cap"/>   
                               </div>
                                  </div>
                       <div className="col-md-3">
                          <div className="card mb-3 box-shadow">
                              <img className="card-img-top" src={img_film2} height="" alt="Card image cap"/>
                          </div>
                        </div>
                        <div className="col-md-3">
                          <div className="card mb-3 box-shadow">
                              <img className="card-img-top" src={img_film3} height="" alt="Card image cap"/>
                          </div>
                        </div>
                        <div className="col-md-3">
                          <div className="card mb-3 box-shadow">
                              <img className="card-img-top" src={img_film4} height="" alt="Card image cap"/>
                        </div>
                      </div>
                    </div>
                  </div>
                </main>
                <br></br>
            </div>
        );
    }
    
}

export default Home;




