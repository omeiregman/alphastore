import React from 'react';
import './css/main.css';

const Footer = () => {
    return(
        <footer>
            <p> © 2018 alphastores.com</p>
        </footer>
    );
}

export default Footer;